<?php

namespace Nicordev\Paginator\Tests;

use PHPUnit\Framework\TestCase;
use Nicordev\Paginator\Paginator;

class PaginatorTest extends TestCase
{
    public function testFitCurrentPageInBoundaries_positiveOutbound(): void
    {
        $paginator = new Paginator();
        $paginator->pagesCount = 20;
        $paginator->currentPage = 21;
        $paginator->fitCurrentPageInBoundaries();
        $this->assertEquals(20, $paginator->currentPage);
    }

    public function testFitCurrentPageInBoundaries_negativeOutbound(): void
    {
        $paginator = new Paginator();
        $paginator->currentPage = -1;
        $paginator->fitCurrentPageInBoundaries();
        $this->assertEquals(1, $paginator->currentPage);
    }

    public function testUpdate_currentPagePositiveOutbound(): void
    {
        $paginator = new Paginator();
        $itemsPerPage = 10;
        $itemsCount = 100;
        $paginator->update(
            1000,
            $itemsPerPage,
            $itemsCount,
            true
        );
        $this->assertEquals(10, $paginator->currentPage);
        $this->assertEquals(10, $paginator->nextPage);
        $this->assertEquals(9, $paginator->previousPage);
    }

    public function testUpdate_currentPageNegativeOutbound(): void
    {
        $paginator = new Paginator();
        $itemsPerPage = 10;
        $itemsCount = 100;
        $paginator->update(
            -5,
            $itemsPerPage,
            $itemsCount,
            true
        );
        $this->assertEquals(1, $paginator->currentPage);
        $this->assertEquals(2, $paginator->nextPage);
        $this->assertEquals(1, $paginator->previousPage);
    }

    public function testUpdate_itemsPerPageSuperiorThanItemsCount(): void
    {
        $paginator = new Paginator();
        $itemsPerPage = 10000;
        $itemsCount = 100;
        $paginator->update(
            1,
            $itemsPerPage,
            $itemsCount,
            true
        );
        $this->assertEquals(1, $paginator->currentPage);
        $this->assertEquals(1, $paginator->nextPage);
        $this->assertEquals(1, $paginator->previousPage);
        $this->assertEquals(100, $paginator->itemsPerPage);
    }

    public function testUpdate_negativeItemsPerPage(): void
    {
        $paginator = new Paginator();
        $itemsPerPage = -1000;
        $itemsCount = 100;
        $paginator->update(
            1,
            $itemsPerPage,
            $itemsCount,
            true
        );
        $this->assertEquals(1, $paginator->currentPage);
        $this->assertEquals(2, $paginator->nextPage);
        $this->assertEquals(1, $paginator->previousPage);
        $this->assertEquals(1, $paginator->itemsPerPage);
    }
}