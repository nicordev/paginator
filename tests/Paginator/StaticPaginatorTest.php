<?php

namespace Nicordev\Paginator\Tests;

use PHPUnit\Framework\TestCase;
use Nicordev\Paginator\StaticPaginator;

class StaticPaginatorTest extends TestCase
{
    public function testCalculateNextPageNumber_inbound(): void
    {
        $pagesCount = 20;
        $currentPage = 15;
        $nextPage = StaticPaginator::calculateNextPageNumber($currentPage, $pagesCount);
        $this->assertEquals($currentPage + 1, $nextPage);
    }

    public function testCalculateNextPageNumber_positiveOutbound(): void
    {
        $pagesCount = 20;
        $currentPage = 4000;
        $nextPage = StaticPaginator::calculateNextPageNumber($currentPage, $pagesCount);
        $this->assertEquals($pagesCount, $nextPage);
    }

    public function testCalculateNextPageNumber_negativeOutbound(): void
    {
        $pagesCount = 20;
        $currentPage = -4000;
        $nextPage = StaticPaginator::calculateNextPageNumber($currentPage, $pagesCount);
        $this->assertEquals(1, $nextPage);
    }

    public function testCalculatePreviousPageNumber_inbound(): void
    {
        $pagesCount = 20;
        $currentPage = 15;
        $previousPage = StaticPaginator::calculatePreviousPageNumber($currentPage, $pagesCount);
        $this->assertEquals($currentPage - 1, $previousPage);
    }

    public function testCalculatePreviousPageNumber_positiveOutbound(): void
    {
        $pagesCount = 20;
        $currentPage = 4000;
        $previousPage = StaticPaginator::calculatePreviousPageNumber($currentPage, $pagesCount);
        $this->assertEquals($pagesCount, $previousPage);
    }

    public function testCalculatePreviousPageNumber_negativeOutbound(): void
    {
        $pagesCount = 20;
        $currentPage = -4000;
        $previousPage = StaticPaginator::calculatePreviousPageNumber($currentPage, $pagesCount);
        $this->assertEquals(1, $previousPage);
    }
}