# Paginator

Pagination helper class.

## Usage

Look in `tests/Paginator` to figure out how to use it, either statically or through instantiation.

## Test

```bash
composer install
php vendor/bin/phpunit tests
```

Have a nice day!