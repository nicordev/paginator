<?php

require_once __DIR__.'/vendor/autoload.php';

use Nicordev\Paginator\Paginator;
use Nicordev\Paginator\StaticPaginator;

$itemsCount = 30;
$itemsPerPage = 5;
$pagesCount = StaticPaginator::countPages($itemsCount, $itemsPerPage);

echo "$pagesCount\n";

echo StaticPaginator::calculateNextPageNumber(4, $pagesCount)."\n";
echo StaticPaginator::calculateNextPageNumber(8, $pagesCount)."\n";
echo StaticPaginator::calculatePreviousPageNumber(0, $pagesCount)."\n";

$paginator = new Paginator();

$paginator->update(3, $itemsPerPage, $itemsCount, true);

echo $paginator->currentPage."\n";

$paginator->currentPage = -1;
$paginator->update();
echo $paginator->currentPage."\n";