<?php

namespace Nicordev\Paginator;

class StaticPaginator
{
    /**
     * Give the correct page number within the boundaries
     */
    public static function applyBoundaries(int $page, int $min, int $max)
    {
        if ($page < $min) {
            return $min;
        }

        if ($page > $max) {
            return $max;
        }

        return $page;
    }

    public static function calculateNextPageNumber(int $currentPage, int $pagesCount): int
    {
        return self::applyBoundaries($currentPage + 1, 1, $pagesCount);
    }

    public static function calculatePreviousPageNumber(int $currentPage, int $pagesCount): int
    {
        return self::applyBoundaries($currentPage - 1, 1, $pagesCount);
    }

    public static function calculatePagingOffset(int $page, int $linesPerPage): int
    {
        return ($page - 1) * $linesPerPage;
    }

    public static function countPages(int $itemsCount, int $itemsPerPage): int
    {
        return ceil($itemsCount / $itemsPerPage);
    }
}