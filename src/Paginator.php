<?php

namespace Nicordev\Paginator;

class Paginator
{
    public int $currentPage;
    public int $nextPage;
    public int $previousPage;
    public int $itemsPerPage;
    public int $itemsCount;
    public int $pagesCount;
    public int $pagingOffset;

    public function __construct(
        ?int $currentPage = null,
        ?int $itemsPerPage = null,
        ?int $itemsCount = null,
        bool $fitCurrentPageInBoundaries = true
    ) {
        $this->update(
            $currentPage,
            $itemsPerPage,
            $itemsCount,
            $fitCurrentPageInBoundaries
        );
    }

    /**
     * Update the attributes all at once
     */
    public function update(
        ?int $currentPage = null,
        ?int $itemsPerPage = null,
        ?int $itemsCount = null,
        bool $fitCurrentPageInBoundaries = true
    ) {
        if ($currentPage) {
            $this->currentPage = $currentPage;
        }
        if ($itemsPerPage) {
            if ($itemsPerPage < 1) {
                $itemsPerPage = 1;
            }
            $this->itemsPerPage = $itemsPerPage;
        }
        if ($itemsCount) {
            $this->itemsCount = $itemsCount;
        }
        if ($itemsCount && $itemsPerPage) {
            if ($itemsPerPage > $itemsCount) {
                $this->itemsPerPage = $itemsCount;
            }
            $this->pagesCount = StaticPaginator::countPages($this->itemsCount, $this->itemsPerPage);
            if ($fitCurrentPageInBoundaries) {
                $this->fitCurrentPageInBoundaries();
            }
        }
        if ($currentPage && $itemsPerPage) {
            $this->pagingOffset = StaticPaginator::calculatePagingOffset($this->currentPage, $itemsPerPage); // Use of the object's attribute to stay in boundaries
        }
        if ($currentPage && $this->pagesCount) {
            $this->nextPage = StaticPaginator::calculateNextPageNumber($this->currentPage, $this->pagesCount);
            $this->previousPage = StaticPaginator::calculatePreviousPageNumber($this->currentPage, $this->pagesCount);
        }
    }

    /**
     * Set the current page within paging count
     */
    public function fitCurrentPageInBoundaries()
    {
        $this->currentPage = StaticPaginator::applyBoundaries($this->currentPage, 1, $this->pagesCount ?? $this->currentPage);
    }
}